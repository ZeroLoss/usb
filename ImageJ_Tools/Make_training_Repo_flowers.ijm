// Haartraining 
/* 

This script is intended to be used with images (JPG) of containing objects of interest (flowers, buds ,fruit)
Objects of interest are subset to generate positive and negative training sets
Author: Peter McAtee (peter.mcatee@plantandfood.co.nz)

*/

//Set directory to the current directory you are working in
dir = getDirectory("Please choose a directory containing images");
list = getFileList(dir);
Array.sort(list);
wait(300);

//Define the 
myObjOI = getString("Please input the name of the positive object you are interested in", "Flower_Class1");

//Get Time and Date
getDateAndTime(year, month, dayOfWeek, dayOfMonth, hour, minute, second, msec);

myDateStmp = toString(year)+"."+toString(month)+"."+toString(dayOfMonth);

//Set the time
	if (hour<10) {hour = "0"+hour;}
	if (minute<10) {minute = "0"+minute;}
	if (second<10) {second = "0"+second;}
File.append("Time started-" +hour+":"+minute+ ":"+second, dir+"LOGFILE.txt");
print("Bud selector tool started at- " +hour+":"+minute+ ":"+second);

//Make a temporary directory to write Positive files to
myDir = dir+myObjOI+"_Positives/";
//Make a new temporary directory 	
File.makeDirectory(myDir);

myDir2 = dir+myObjOI+"_Negatives/";
//Make a new temporary directory 	
File.makeDirectory(myDir2);

	for (i=0;i<list.length; i++){
		if (endsWith(list[i],"jpg") || endsWith(list[i],"JPG")){
			path = dir+list[i];
			open(path);

			run("Enhance Contrast...", "saturated=0.3 equalize");

			label = list[i];
			newlabel = substring(list[i], 0, lengthOf(list[i])-4);

			Dialog.create("Awaiting user input");
			Dialog.addChoice("Is the image suitable", newArray("Yes", "No"), "Yes");
			Dialog.addNumber("Approximate number of Objects of interest", 5);
			Dialog.show();

			myChoice = Dialog.getChoice();
			myNumObj = Dialog.getNumber();


			if (myChoice=="Yes"){

				File.append("Training images subset from "+label, dir+"LOGFILE.txt");
				
				//Make ten positive bud selections per image		
				for(iPos=0; iPos < myNumObj; iPos++){

					if (iPos+1 <10) {iPosNum = "0"+iPos+1;}

					selectWindow(label);

					myPosImg_ID = newlabel+myDateStmp+"IMG__POS_"+iPosNum+".jpg";
					PosName = "Img_Selection";

					print("Positive selection #"+iPos+1+" for image "+label);

					//Set 350 by 350 window to make OI selection. Positives selection ROI is set to yellow
					run("Colors...", "foreground=black background=white selection=yellow");
					makeRectangle(1000, 1000, 350, 350);

					waitForUser("User Action required: Position and resize selection","Position rectangle over positive bud examples then click okay");

					setBatchMode(true);

					w = 0; // Set the width of the rectangular selection to 0
					h = 0; // Set the height of the rectangular selection to 0
					getSelectionBounds(x,y,w,h); // Get the height and width of the selection 

					myMinDim = minOf(w, h);

					run("Copy");
					newImage(PosName, "RGB black", myMinDim, myMinDim, 1);
					run("Paste");

					//myW_Scale_factor = 0; myH_Scale_factor = 0;

					//myW_Scale_factor = 50/myMinDim;
					//myH_Scale_factor = 50/myMinDim;

					//myScaleCMD = "x="+myW_Scale_factor+" y="+myH_Scale_factor+" interpolation=Bicubic average create title=[Scaled image]";
					//run("Scale...", myScaleCMD);

					//selectWindow("Scaled image");
					myImgWidth = 0; // Set the width of the scaled image 0
					myImgHeight = 0; // Set the height of the scaled image 0
					myImgWidth = getWidth();
					myImgHeight = getHeight();

					saveAs("Jpeg", myDir+myPosImg_ID);
					File.append(myDir+myPosImg_ID+" "+"1 "+"0 "+"0 "+myImgWidth+" "+myImgHeight, dir+"positives.txt");
					
					selectWindow(myPosImg_ID);
					close();
					
					//selectWindow(PosName);
					//close();

					setBatchMode(false);
				
				}


				for(iNeg=0; iNeg < myNumObj; iNeg++){

					if (iNeg+1 <10) {iNegNum = "0"+iNeg+1;}

					selectWindow(label);

					myNegImg_ID = newlabel+myDateStmp+"IMG_NEG_"+iNeg+".jpg";
					NegName = "Img_Selection";

					print("Negative selection #"+iNeg+1+" for image "+label);

					//Set 350 by 350 window to make OI selection. Negatives selection ROI is set to red
					run("Colors...", "foreground=black background=white selection=red");
					makeRectangle(1000, 1000, 350, 350);

					waitForUser("User Action required","Position rectangle over negative bud examples then click okay");

					setBatchMode(true);

					w = 0; // Set the width of the rectangular selection to 0
					h = 0; // Set the hieght of the rectangular selection to 0
					getSelectionBounds(x,y,w,h); // Get the height and width of the selection 

					myMinDim = minOf(w, h);
			
					run("Copy");
					newImage(NegName, "RGB black", myMinDim, myMinDim, 1);
					run("Paste");

					//myW_Scale_factor = 0; myH_Scale_factor = 0;

					//myW_Scale_factor = 50/myMinDim;
					//myH_Scale_factor = 50/myMinDim;

					//myScaleCMD = "x="+myW_Scale_factor+" y="+myH_Scale_factor+" interpolation=Bicubic average create title=[Scaled image]";
					//run("Scale...", myScaleCMD);
					
					//selectWindow("Scaled image");
					//myImgWidth = 0; // Set the width of the scaled image 0
					//myImgHeight = 0; // Set the height of the scaled image 0
					//myImgWidth = getWidth();
					//myImgHeight = getHeight();
					
					saveAs("Jpeg", myDir2+myNegImg_ID);
					File.append(myDir2+myNegImg_ID , dir+"negatives.txt");
					
	
					selectWindow(myNegImg_ID);
					close();
					
					//selectWindow(NegName);
					//close();

					setBatchMode(false);
				}

			}

		selectWindow(label);
		close();
			
		
		}
	}

//Get Time and Date
getDateAndTime(year, month, dayOfWeek, dayOfMonth, hour, minute, second, msec);

//Set the time
	if (hour<10) {hour = "0"+hour;}
	if (minute<10) {minute = "0"+minute;}
	if (second<10) {second = "0"+second;}
File.append("Time ended-" +hour+":"+minute+ ":"+second, dir+"LOGFILE.txt");
print("Bud selector tool ended at- " +hour+":"+minute+ ":"+second);
			
			