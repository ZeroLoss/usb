


run("Sharpen"); // _Sharp
run("Smooth"); //_Smooth
run("Salt and Pepper"); // _SnP
run("Add Noise"); // _Noise
run("North"); // _ShadowN
run("West"); // _ShadowW
run("Southeast"); //_ ShadowSE
run("Enhance Contrast...", "saturated=5 equalize"); // Contrast5
run("Enhance Contrast...", "saturated=20"); // Contrast20
run("Enhance Contrast...", "saturated=20 equalize"); // Contrast20eq
run("Rotate 90 Degrees Right"); // Rot90
run("Flip Vertically"); //FlipV
run("Flip Horizontally"); // FlipH