
## 1 ) Move to the directory containing the negative images and run the following command to make a text file listing the absolute path

cd /Users/petermcatee/Desktop/Flower_Class1_Negatives/

find /Users/petermcatee/Desktop/Flower_Class1_Negatives -type f > negatives.txt

2) ## You now need to generate a file containing the information (filepath and size/number) of positive objects in each positive image

# The format of the text file is: "Absolute path to image” “Number of positive objects” “X origin” “Y origin” “Width” “Height" e.g. ../IMG_16782017.11.19IMG__POS_03.jpg 1 0 0 50 50
# IF multiple positive objects the x, y, width and height of the secondary object are placed in series”
# Note: Larger images require more compute resources. Scaling down the image size saves time in the training process.
# To get the absolute path for all the positive images in a directory I used the following command on MAC

cd /Users/petermcatee/Desktop/Flower_Class1_Positives/

find /Users/petermcatee/Desktop/Flower_Class1_Positives -type f > positive_filePath.txt

## I then manually added the image number, x and y origins and the width and height in excel and saved the file as a tab delimited text file called “PosInfo.txt”


3) ## Generate vector file to facilitate training
# This will create the vector files that are used to train the cascade. I believe the -w and -h are a absolute scaling factor (down) to save compute time

opencv_createsamples -info PosInfo.txt -vec Flowering_Positives.vec -num 500 -w 50 -h 50

# When successful the print out will look similar to this

####

Info file name: PosInfo.txt
Img file name: (NULL)
Vec file name: /Users/petermcatee/Desktop/Bub_Break/114___09/BudClass1_Positives/Bud_Break_Positives.vec
BG  file name: (NULL)
Num: 252
BG color: 0
BG threshold: 80
Invert: FALSE
Max intensity deviation: 40
Max x angle: 1.1
Max y angle: 1.1
Max z angle: 0.5
Show samples: FALSE
Width: 50
Height: 50
Max Scale: -1
Create training samples from images collection...
Done. Created 252 samples


####

3.1) ## You can view that the ‘.vec’ file was generated properly with the following command. This will open a window that shows the binarised positive object images

opencv_createsamples -vec Flowering_Positives.vec -w 50 -h 50



#######
#######


## 4) Train the cascade

#Note: -numPos should be 0.9 * ‘.vec’ file. This is because some will be excluded during the training process

wc -l negatives.txt # Obtain the number of lines in the negatives.txt file is will equal the number of negative images
wc -l positives.txt # Obtain the number of lines in the positives.txt file is will equal the number of positives images

mkdir Haarcascade

opencv_traincascade -data Haarcascade -vec Bud_Break_Positives.vec -bg negatives.txt -featureType LBP -numPos 480 -numNeg 532 -w 50 -h 50 -nsplits 2 -nstages 20 -minhitrate 0.999 -maxfalsealarm 0.5

# When successful the print out will look similar to this

####

PARAMETERS:
cascadeDirName: BudCascadeTraining_Out
vecFileName: Bud_Break_Positives.vec
bgFileName: negatives.txt
numPos: 226
numNeg: 261
numStages: 20
precalcValBufSize[Mb] : 1024
precalcIdxBufSize[Mb] : 1024
acceptanceRatioBreakValue : -1
stageType: BOOST
featureType: HAAR
sampleWidth: 50
sampleHeight: 50
boostType: GAB
minHitRate: 0.995
maxFalseAlarmRate: 0.5
weightTrimRate: 0.95
maxDepth: 1
maxWeakCount: 100
mode: BASIC
Number of unique features given windowSize [50,50] : 3024775

===== TRAINING 0-stage =====
<BEGIN
POS count : consumed   226 : 226
NEG count : acceptanceRatio    261 : 1
Precalculation time: 7
+----+---------+---------+
|  N |    HR   |    FA   |
+----+---------+---------+
|   1|        1|        1|
+----+---------+---------+
|   2|        1|        1|
+----+---------+---------+
|   3|        1| 0.827586|
+----+---------+---------+
|   4|        1| 0.708812|
+----+---------+---------+
|   5|        1| 0.720307|
+----+---------+---------+
|   6|        1| 0.747126|
+----+---------+---------+
|   7| 0.995575|  0.64751|
+----+---------+---------+
|   8| 0.995575| 0.670498|
+----+---------+---------+
|   9| 0.995575| 0.570881|
+----+---------+---------+
|  10| 0.995575| 0.436782|
+----+---------+---------+
END>
Training until now has taken 0 days 0 hours 12 minutes 35 seconds.

===== TRAINING 1-stage =====
<BEGIN
POS count : consumed   226 : 227
NEG count : acceptanceRatio    261 : 0.442373
Precalculation time: 7
+----+---------+---------+
|  N |    HR   |    FA   |
+----+---------+---------+
|   1|        1|        1|
+----+---------+---------+
|   2|        1|        1|
+----+---------+---------+
|   3|        1|        1|
+----+---------+---------+
|   4| 0.995575|  0.83908|
+----+---------+---------+
|   5|        1| 0.888889|
+----+---------+---------+
|   6| 0.995575| 0.808429|
+----+---------+---------+
|   7| 0.995575| 0.754789|
+----+---------+---------+
|   8| 0.995575|  0.64751|
+----+---------+---------+
|   9| 0.995575| 0.613027|
+----+---------+---------+
|  10| 0.995575|  0.37931|
+----+---------+---------+
END>
Training until now has taken 0 days 0 hours 25 minutes 49 seconds.

===== TRAINING 2-stage =====
<BEGIN
POS count : consumed   226 : 229
NEG count : acceptanceRatio    261 : 0.177551
Precalculation time: 9
+----+---------+---------+
|  N |    HR   |    FA   |
+----+---------+---------+
|   1|        1|        1|
+----+---------+---------+
|   2|        1|        1|
+----+---------+---------+
|   3|        1|        1|
+----+---------+---------+
|   4|        1|        1|
+----+---------+---------+
|   5| 0.995575| 0.563218|
+----+---------+---------+
|   6|        1| 0.777778|
+----+---------+---------+
|   7| 0.995575| 0.329502|
+----+---------+---------+
END>
Training until now has taken 0 days 0 hours 34 minutes 20 seconds.

===== TRAINING 3-stage =====
<BEGIN
POS count : consumed   226 : 230
NEG count : acceptanceRatio    261 : 0.0574637
Precalculation time: 6
+----+---------+---------+
|  N |    HR   |    FA   |
+----+---------+---------+
|   1|        1|        1|
+----+---------+---------+
|   2|        1|        1|
+----+---------+---------+
|   3|        1|        1|
+----+---------+---------+
|   4|        1| 0.463602|
+----+---------+---------+
END>
Training until now has taken 0 days 0 hours 38 minutes 45 seconds.

===== TRAINING 4-stage =====
<BEGIN
POS count : consumed   226 : 230
NEG count : acceptanceRatio    261 : 0.0268105
Precalculation time: 6
+----+---------+---------+
|  N |    HR   |    FA   |
+----+---------+---------+
|   1|        1|        1|
+----+---------+---------+
|   2|        1|        1|
+----+---------+---------+
|   3|        1|        0|
+----+---------+---------+
END>
Training until now has taken 0 days 0 hours 41 minutes 59 seconds.


####

## http://note.sonots.com/SciSoftware/haartraining.html ## Nice site

## https://docs.google.com/document/d/14r34Pd51lKZNlfJIQVRS_3kbow1OcJBKV7wTRRAW5Vg/preview ## Good document on training program usage

## http://coding-robin.de/2013/07/22/train-your-own-opencv-haar-classifier.html ##another good site

###http://www.bogotobogo.com/python/OpenCV_Python/python_opencv3_Image_Object_Detection_Face_Detection_Haar_Cascade_Classifiers.php

#####
