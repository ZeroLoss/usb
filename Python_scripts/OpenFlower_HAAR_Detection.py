#!/usr/bin/python

import cv2
import numpy as np
from matplotlib import pyplot as plt

# Cascade item name
#CASCADE_ITEM = 'KF bud'

openflower_cascade = cv2.CascadeClassifier('/Users/petermcatee/Desktop/OrchardScan/Images_Flowering/haar/cascade.xml')

img = cv2.imread('/Users/petermcatee/Desktop/Python/Flowers/IMG_3917.JPG',1) # Open Image in RGB colour

##Scale image to improve performance
org_height, org_width = img.shape[:2]
#print 'The height of the original image is:', org_height,'\n', 'The width of the original image is:',  org_width

myScaleFactor = 0.2

sclImg = cv2.resize(img ,None,fx=myScaleFactor, fy=myScaleFactor, interpolation = cv2.INTER_CUBIC) #interpolation = cv2.INTER_AREA
scl_height, scl_width = sclImg.shape[:2]
#print 'The height of the scaled image is:', scl_height,'\n', 'The width of the scaled image is:',  scl_width

scl_gray = cv2.cvtColor(sclImg, cv2.COLOR_BGR2GRAY)

#Detect bud objects
openFlowers = openflower_cascade.detectMultiScale(scl_gray, scaleFactor=1.3, minNeighbors=2)

#print openFlowers.shape # Print the shape of the buds data array
print 'The number of potential open flowers is:', openFlowers.shape[0] # Print the number of rows (This will be the number of buds detected)
#print 'The numbber of columns is:' , openFlowers.shape[1] # Print the number of columns. There should be four X, Y, width, height


for (x,y,w,h) in openFlowers:
   cv2.rectangle(sclImg,(x,y),(x+w,y+h),(255,0,0),2)
   roi_gray = scl_gray[y:y+h, x:x+w]
   roi_color = sclImg[y:y+h, x:x+w]
   #cv2.putText(img, CASCADE_ITEM + " #{}".format(buds + 1), (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.55, (0, 0, 255), 2)

cv2.imshow('Open flowers detection output',sclImg)
cv2.waitKey(0)
cv2.destroyAllWindows()
